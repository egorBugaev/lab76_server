const fs = require('fs');
const nanoid = require("nanoid");


let data = null;


module.exports = {
	init: () => {
		return new Promise((resolve, reject) => {
			fs.readFile('./db.json', (err, result) => {
				if (err) {
					reject(err);
				} else {
					data = JSON.parse(result);
					resolve();
				}
			});
		});
	},
	getData: () => data,
	getDataByCount: count => {
		if (count >= data.length) {
			return data
		} else {
			let answer = [];
			for (let i = count; i > 0; i--) {
				answer.push(data[data.length - i])
			}
			return answer;
		}
	},
	getDataByDate: date => {
		let dt = new Date(date);
		let answer = [];
		data.forEach((item) => {
			let itemDate = new Date(item.date);
			if (itemDate > dt) {
				answer.push(item)
			}
		});
		return answer
	},
	addItem: (item) => {
		const date = new Date();
		item.id = nanoid();
		item.date = date.toISOString();
		data.push(item);
		
		let contents = JSON.stringify(data, null, 2);
		
		return new Promise((resolve, reject) => {
			fs.writeFile('./db.json', contents, err => {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		});
	}
};