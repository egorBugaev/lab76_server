const express = require('express');
const router = express.Router();

const createRouter = (db) => {
	
	router.get('/all', (req, res) => {
		res.send(db.getData());
	});
	
	
	router.post('/', (req, res) => {
		const message = req.body;
		if (!message.author || !message.message || message.author === '' || message.message === '') {
			res.send({error: "message and author must be"});
			res.status(400);
		} else {
			db.addItem(message).then(result => {
				res.send(result);
			});
		}
	});
	
	
	router.get('/?', (req, res) => {
		if (req.query.count) {
			if (isNaN(req.query.count)) {
				res.status(400);
				res.send({error: "query must be a number"})
			} else {
				res.send(db.getDataByCount(req.query.count));
			}
		} else if (req.query.date) {
			const date = new Date(req.query.date);
			if (isNaN(date.getDate())) {
				res.status(400);
				res.send({error: "date must be in ISOString"})
			} else {
				res.send(db.getDataByDate(req.query.date));
			}
		} else {
			res.status(400);
			res.send({error: "incorrect query params"});
		}
	});
	
	return router;
};

module.exports = createRouter;